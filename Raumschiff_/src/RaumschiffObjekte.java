import java.util.ArrayList;

public class RaumschiffObjekte {
	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int lebenserhaltungsystemInProzent;
	private int androidenNazahl;
	private String schiffsname;
	private ArrayList<String> broadcastKommunikator;
	private ArrayList<Ladung> landungsverzeichnis = new ArrayList<Ladung>();
	
	public RaumschiffObjekte(int photonentorpedoAnzahl, int energieversorgungInPrizent, int schildeInProzent,
			int lebenserhaltungsystemInProzent, int androidenNazahl, String schiffsname) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgungInPrizent;
		this.schildeInProzent = schildeInProzent;
		this.lebenserhaltungsystemInProzent = lebenserhaltungsystemInProzent;
		this.androidenNazahl = androidenNazahl;
		this.schiffsname = schiffsname;
		this.broadcastKommunikator = new ArrayList<String>();
		this.landungsverzeichnis = new ArrayList<Ladung>();
	}
	
	public RaumschiffObjekte(int i, int j, int k, int l, int m, String string, ArrayList<Ladung> arrayList) {
	}
	
	public int getPhotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}

	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}
	
	public int getEnergieversorgungInPrizent() {
		return energieversorgungInProzent;
	}
	
	public void setEnergieversorgungInPrizent(int energieversorgungInPrizent) {
		this.energieversorgungInProzent = energieversorgungInPrizent;
	}

	public int getSchildeInProzent() {
		return schildeInProzent;
	}
	
	public void setSchildeInProzent(int schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
	}
	
	public int getLebenserhaltungsystemInProzent() {
		return lebenserhaltungsystemInProzent;
	}
	
	public void setLebenserhaltungsystemInProzent(int lebenserhaltungsystemInProzent) {
		this.lebenserhaltungsystemInProzent = lebenserhaltungsystemInProzent;
	}
	
	public int getAndroidenNazahl() {
		return androidenNazahl;
	}
	
	public void setAndroidenNazahl(int androidenNazahl) {
		this.androidenNazahl = androidenNazahl;
	}
	
	public String getSchiffsname() {
		return schiffsname;
	}
	
	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}

	public ArrayList<String> getBroadcastKommunikator() {
		return broadcastKommunikator;
	}
	
	public void setBroadcastKommunikator(ArrayList<String> broadcastKommunikator) {
		this.broadcastKommunikator = broadcastKommunikator;
	}
	
	public ArrayList<Ladung> getLandungsverzeichnis() {
		return landungsverzeichnis;
	}	
	
	public void setLandungsverzeichnis(ArrayList<Ladung> landungsverzeichnis) {
		this.landungsverzeichnis = landungsverzeichnis;
	}

	@Override
	public String toString() {
		return "Androiden Azahl = " + androidenNazahl + ", \nBroadcastkommunikator = "
				+ broadcastKommunikator + ", \nEnergieversorgung = " + energieversorgungInProzent + "%"
				+ ", \nLandungsverzeichnis = " + landungsverzeichnis + ", \nLebenserhaltungsystem = "
				+ lebenserhaltungsystemInProzent + "%" + ", \nphotonentorpedoAnzahl = " + photonentorpedoAnzahl
				+  ", \nSchiffsname = " + schiffsname + ", \nSchild = "
				+ schildeInProzent + "%";
	}		
}