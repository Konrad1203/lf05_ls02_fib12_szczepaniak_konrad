public class Ladung {
	private String bezeichnung;
	private int menge;

	public Ladung() {
		System.out.println("Standart");
	}

	public Ladung(String bezeichnung, int menge) {
		this.bezeichnung = bezeichnung;
		this.menge = menge;
	}
	
	public String getBezeichnung() {
		return bezeichnung;
	}
	
	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	} 

	public int getMenge() {
		return menge;
	}
	
	public void setMenge(int menge) {
		this.menge = menge;
	}

	@Override
	public String toString() {
		return "[Bezeichnung = " + bezeichnung + "\n" +", Menge = " + menge + "]";
	}
	
	Ladung bieb1 = new Ladung("Plasma Waffe", 50);
	Ladung bieb2 = new Ladung("Photonentorpedo", 3);
	Ladung bieb3 = new Ladung("Klingonen Schwert", 200);
	Ladung bieb4 = new Ladung("Ferengi", 200);
	Ladung bieb5 = new Ladung("Rote Materie", 2);
	Ladung bieb6 = new Ladung("Forschungsonde", 35);
	Ladung bieb7 = new Ladung("Borg-Schrott", 5);
}
